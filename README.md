WASD -> moverse

Space -> saltar

X -> alternar agacharse

Q -> celebración

E -> usar

Mantener click derecho -> apuntar

Click izquierdo (no mantener) -> disparar

(La animación de disparar es un poco disimulada. Pasa tanto al andar como al saltar como al estar quieto, pero sobre todo se nota al estar quieto)
