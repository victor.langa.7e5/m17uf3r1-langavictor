using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerAnimation))]
[RequireComponent(typeof(CharacterController))]
public class Player : MonoBehaviour
{
    private static Player _instance;
    public static Player Instance { get { return _instance; } }


    private enum EPlayerState
    { 
        NONE,
        STANDARD,
        LOCKED,
        JUMPING,
        AIMING,
        JUMPING_AND_AIMING
    }

    [SerializeField] private CharacterController _characterController;
    [SerializeField] private PlayerAnimation _playerAnimation;
    private EPlayerState _state = EPlayerState.STANDARD;

    [SerializeField] private GameObject _standardCamera, _celebrationCamera, _aimCamera;

    [Space]
    [SerializeField] private float _speedMultiplier;
    private Vector3 _inputSpeed;
    private Vector3 _currentSpeed;

    private bool _isJumpAvailable = true;
    [SerializeField] private float _jumpHeight;

    private bool _isCrouching = false;
    private bool _isAiming = false;
    private Coroutine _aimCoroutine;

    [Space]
    private Vector2 _aimRotation;
    [SerializeField] private float _limitRotationAngle;
    [SerializeField] private float _cameraSensitivityRatio;

    private void Awake()
    {
        if (_instance != this && _instance != null)
            Destroy(gameObject);
        _instance = this;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        if (_state != EPlayerState.LOCKED)
        {
            switch (_state)
            {
                case EPlayerState.STANDARD:
                    Vector3 direction = new(_characterController.velocity.x, 0, _characterController.velocity.z);
                    if (direction.magnitude > 0.5f)
                        transform.rotation = Quaternion.LookRotation(direction);
                    break;
                case EPlayerState.AIMING:
                case EPlayerState.JUMPING_AND_AIMING:
                    transform.rotation = Quaternion.Euler(0, _aimRotation.x, -_aimRotation.y);
                    break;
            }

            _currentSpeed = new Vector3(Mathf.Lerp(_currentSpeed.x, _inputSpeed.x, Time.deltaTime), _currentSpeed.y, Mathf.Lerp(_currentSpeed.z, _inputSpeed.z, Time.deltaTime));
            _inputSpeed.y -= _characterController.isGrounded ? 0 : 9.81f * Time.deltaTime;

            _characterController.Move(_speedMultiplier * Time.deltaTime * (Camera.main.transform.TransformDirection(_currentSpeed) + new Vector3(0, _inputSpeed.y, 0)));
        }

        _playerAnimation.SetHorizontalSpeed(_currentSpeed);
        _isAiming = _playerAnimation.GetShootLayerWeight() == 1;
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        if (_state == EPlayerState.LOCKED)
            return;

        if (context.performed)
        {
            Vector2 value = context.ReadValue<Vector2>();
            _inputSpeed = new Vector3(value.x, _inputSpeed.y, value.y);
        }

        if (context.canceled)
            _inputSpeed = new Vector3(0, _inputSpeed.y, 0);
    }

    public void OnCrouch(InputAction.CallbackContext context)
    {
        if (context.performed && _state == EPlayerState.STANDARD)
        {
            _isCrouching = !_isCrouching;
            _playerAnimation.SetIsCrouching(_isCrouching);
        }
    }

    public void OnTake(InputAction.CallbackContext context)
    {
        if (context.performed && _state != EPlayerState.LOCKED)
        {
            LockPlayerState();
            _playerAnimation.TriggerTake();
        }
    }

    public void OnCelebrate(InputAction.CallbackContext context)
    {
        if (context.performed && _state != EPlayerState.LOCKED)
        {
            LockPlayerState();
            _standardCamera.SetActive(false);
            _celebrationCamera.SetActive(true);
            _playerAnimation.TriggerCelebrate();
        }
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        if (context.performed && _isJumpAvailable && _state != EPlayerState.LOCKED)
        {
            switch (_state)
            { 
                case EPlayerState.STANDARD:
                    _state = EPlayerState.JUMPING;
                    break;
                case EPlayerState.AIMING:
                    _state = EPlayerState.JUMPING_AND_AIMING;
                    break;
            }
            
            _isJumpAvailable = false;
            _playerAnimation.TriggerJump();
            StartCoroutine(WaitForJump());
        }
    }

    private IEnumerator WaitForJump()
    {
        yield return new WaitForSeconds(0.6f);
        _inputSpeed.y = _jumpHeight;

        yield return null;
        StartCoroutine(ResetJumpAvailable());
    }

    private IEnumerator ResetJumpAvailable()
    { 
        while (!_characterController.isGrounded)
            yield return null;

        yield return new WaitForSeconds(0.5f);

        switch (_state)
        {
            case EPlayerState.JUMPING:
                _state = EPlayerState.STANDARD;
                break;
            case EPlayerState.JUMPING_AND_AIMING:
                _state = EPlayerState.AIMING;
                break;
        }

        _isJumpAvailable = true;
    }

    public void OnAim(InputAction.CallbackContext context)
    {
        if (context.performed && _state != EPlayerState.LOCKED && !_isCrouching)
        {
            _state = EPlayerState.AIMING;
            if (_aimCoroutine != null)
                StopCoroutine(_aimCoroutine);
            _aimCoroutine = StartCoroutine(ModifyShootLayerWeight(true));
            _standardCamera.SetActive(false);
            _aimCamera.SetActive(true);
        }

        if (context.canceled)
        {
            if (_aimCoroutine != null)
                StopCoroutine(_aimCoroutine);
            _aimCoroutine = StartCoroutine(ModifyShootLayerWeight(false));

            if (_state != EPlayerState.LOCKED)
            {
                _state = EPlayerState.STANDARD;
                _standardCamera.SetActive(true);
                _aimCamera.SetActive(false);
            }
        }
    }

    private IEnumerator ModifyShootLayerWeight(bool isAddingWeight)
    {
        while (_playerAnimation.GetShootLayerWeight() < 1 || _playerAnimation.GetShootLayerWeight() > 0)
        {
            yield return null;
            _playerAnimation.SetShootLayerWeight(isAddingWeight); 
        }
    }

    public void OnShoot(InputAction.CallbackContext context)
    {
        if (_isAiming && context.performed)
            _playerAnimation.TriggerShoot();
    }

    public void OnLook(InputAction.CallbackContext context)
    {
        if (_state == EPlayerState.AIMING || _state == EPlayerState.JUMPING_AND_AIMING)
        {
            _aimRotation += new Vector2(context.ReadValue<Vector2>().x, context.ReadValue<Vector2>().y) * Time.deltaTime * _cameraSensitivityRatio;
            _aimRotation.y = Mathf.Clamp(_aimRotation.y, -_limitRotationAngle, _limitRotationAngle);
        }
    }

    private void LockPlayerState()
    {
        _currentSpeed = Vector3.zero;
        _inputSpeed = Vector3.zero;
        _state = EPlayerState.LOCKED;
    }

    public void ResetPlayerState()
    {
        _state = EPlayerState.STANDARD;
        _standardCamera.SetActive(true);
        _celebrationCamera.SetActive(false);
        _aimCamera.SetActive(false);
    }

    public CharacterController GetCharacterController() => _characterController;
}
