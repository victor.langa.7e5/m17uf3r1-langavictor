using UnityEngine;
using UnityEngine.InputSystem;

public class EndGame : MonoBehaviour
{
    [SerializeField] private PlayerInput _playerInput;
    [SerializeField] private GameObject _titleText, _buttonReset, _cantEnterText;

    private void OnTriggerEnter(Collider other)
    {
        if (Inventory.Instance.CollectedGems >= 3)
        {
            _playerInput.enabled = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            _titleText.SetActive(true);
            _buttonReset.SetActive(true);
        }
        else
        {
            _cantEnterText.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _cantEnterText.gameObject.SetActive(false);
    }
}
