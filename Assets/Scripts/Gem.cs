using System.Collections;
using UnityEngine;

public class Gem : MonoBehaviour, ICollectable
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private Collider _collider;
    [SerializeField] private AudioClip _onCollectSoundEffect;
    public EGem gemType;

    public enum EGem
    { 
        NONE,
        STAR,
        HEART, 
        EMERALD
    }

    private void Start() => _audioSource.clip = _onCollectSoundEffect;

    public IEnumerator OnCollect()
    {
        _audioSource.Play();
        yield return new WaitForSeconds(_audioSource.clip.length);
        Inventory.Instance.OnGemCollected(gemType);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        _collider.enabled = false;
        StartCoroutine(OnCollect());
    }
}
