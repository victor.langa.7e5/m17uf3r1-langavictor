using System;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    private static Inventory _instance;
    public static Inventory Instance { get { return _instance; } }

    public int CollectedGems { get; private set; }

    private void Awake()
    {
        if (_instance != this && _instance != null)
            Destroy(gameObject);
        _instance = this;

        CollectedGems = 0;
    }

    [Serializable]
    public struct GemGameObject
    { 
        public Gem.EGem Type;
        public GameObject GemUi;
        public GameObject GemPlayer;
    }

    public GemGameObject[] gems;

    public void OnGemCollected(Gem.EGem gemType)
    {
        foreach (var gem in gems)
            if (gem.Type == gemType)
            {
                CollectedGems++;
                gem.GemUi.SetActive(true);
                gem.GemPlayer.SetActive(true);
            }
    }
}
