using System.Collections;

public interface ICollectable
{
    public IEnumerator OnCollect();
}
