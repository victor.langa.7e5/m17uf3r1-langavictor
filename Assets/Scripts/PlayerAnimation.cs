using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerAnimation : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    public void SetHorizontalSpeed(Vector3 speed)
    {
        float tal = new Vector2(speed.x, speed.z).magnitude;
        _animator.SetFloat("HorizontalSpeed", tal);
    }

    public void SetVerticalSpeed(Vector3 speed) => _animator.SetFloat("VerticalSpeed", speed.y);

    public void SetIsCrouching(bool isCrouching) => _animator.SetBool("IsCrouching", isCrouching);

    public void TriggerJump() => _animator.SetTrigger("Jump");

    public void TriggerCelebrate() => _animator.SetTrigger("Celebrate");

    public void TriggerTake() => _animator.SetTrigger("Take");

    public void TriggerShoot() => _animator.SetTrigger("Shoot");

    public void SetShootLayerWeight(bool isAddingWeight) => _animator.SetLayerWeight(1, Mathf.Clamp(_animator.GetLayerWeight(1) + (isAddingWeight ? Time.deltaTime : -Time.deltaTime), 0, 1));

    public float GetShootLayerWeight() => _animator.GetLayerWeight(1);
}
