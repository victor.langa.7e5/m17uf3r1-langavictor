using UnityEngine;

public class ResetPlayerState : StateMachineBehaviour
{
    private Player _player;

    private void Awake() => _player = GameObject.Find("PlayerTest").GetComponent<Player>();

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) => _player.ResetPlayerState();
}
