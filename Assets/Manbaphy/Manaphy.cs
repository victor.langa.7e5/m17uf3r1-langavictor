using UnityEngine;
using UnityEngine.AI;

public class Manaphy : MonoBehaviour
{
    [SerializeField] private NavMeshAgent _navMeshAgent;
    [SerializeField] private Vector3[] _positionsToMoveTo;
    [SerializeField] private Animator _animator;

    private int _currentIndex = 0;
    private float _timeWaiting = 0;
    private EManaphyState _state = EManaphyState.PATROLING;

    private enum EManaphyState
    { 
        NONE, 
        CHASING,
        IDLE,
        PATROLING
    }

    private void Update()
    {
        switch (_state)
        {
            case EManaphyState.CHASING:
                Chasing();
                break;
            case EManaphyState.PATROLING:
                Patroling();
                break;
            case EManaphyState.IDLE:
                Waiting();
                break;
        }

        _animator.SetFloat("HorizontalSpeed", new Vector2(_navMeshAgent.velocity.x, _navMeshAgent.velocity.z).magnitude);
    }

    private void Waiting()
    {
        _timeWaiting += Time.deltaTime;
        _navMeshAgent.destination = transform.position;

        if (_timeWaiting > 5f)
        {
            _timeWaiting = 0;
            _state = EManaphyState.PATROLING;
        }
    }

    private void Patroling()
    {
        if (Vector3.Distance(_positionsToMoveTo[_currentIndex], transform.position) < 1)
        {
            _state = EManaphyState.IDLE;
            _currentIndex = (_currentIndex + 1) % _positionsToMoveTo.Length;
        }

        _navMeshAgent.destination = _positionsToMoveTo[_currentIndex];
    }

    private void Chasing()
    {
        if (Vector3.Distance(Player.Instance.transform.position, transform.position) > 25)
            _state = EManaphyState.PATROLING;

        _navMeshAgent.destination = Player.Instance.transform.position;
    }

    public void Attack()
    {
        _animator.SetTrigger("Attack");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            _state = EManaphyState.CHASING;
    }
}
